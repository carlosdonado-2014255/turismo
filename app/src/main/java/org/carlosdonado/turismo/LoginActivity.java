package org.carlosdonado.turismo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //crear lsa 3 acciones de el boton y de Email y de Password
        Button btnAcceder = (Button) findViewById(R.id.btnAcceder);
        final EditText correo = (EditText) findViewById(R.id.correo);
        final EditText clave = (EditText) findViewById(R.id.clave);

        btnAcceder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, WebService.authenticate, new JSONArray(), new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int x = 0; x < response.length(); x++) {
                                if (response.getJSONObject(x).get("correo").toString().equals(correo.getText().toString())
                                        && response.getJSONObject(x).get("clave").toString().equals(clave.getText().toString())) {
                                    acceso();
                                }
                            }
                        } catch(Exception ex) {
                            Log.e("JSONException", ex.getMessage());
                        }
                    }
                }, new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError err) {
                        Log.e("VolleyError", err.getMessage());
                    }
                });
                WebService.getWebService(view.getContext()).addToRequestQueue(jsonArrayRequest);
            }
        });
    }

    private void acceso() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }
}
